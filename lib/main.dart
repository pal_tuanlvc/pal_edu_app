import 'package:flutter/material.dart';
import 'package:pal_edu_app/screens/login_screen.dart';
import 'package:simple_animations/simple_animations.dart';

import 'animated/snow-animation.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginScreen(title: 'Flutter Demo Home Page'),
    );
  }
}