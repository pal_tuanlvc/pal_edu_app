import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pal_edu_app/components/line.dart';
import 'package:simple_animations/simple_animations.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<LoginScreen>
    with SingleTickerProviderStateMixin {
  bool _isRunning = true;

  @override
  void initState() {
    super.initState();
  }

  //anim
  @override
  Widget build(BuildContext context) {
    return AnimatedBackground();
  }
}

class AnimatedBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            gradient: RadialGradient(
              center: Alignment.center,
              radius: 0.01,
              colors: [
                Color(0xffffffff),
                Color(0xff0c6ebb),
              ],
              stops: [
                0,
                1,
              ],
            ),
            backgroundBlendMode: BlendMode.hardLight,
          ),
          child: PlasmaRenderer(
            type: PlasmaType.bubbles,
            particles: 2000,
            color: Color(0x44ffffff),
            blur: 0,
            size: 0.01,
            speed: 0.5,
            offset: 0,
            blendMode: BlendMode.screen,
            particleType: ParticleType.atlas,
            variation1: 0.1,
            variation2: 0.1,
            variation3: 0.1,
            rotation: 0.5,
          ),
        ),
        Positioned(
          top: MediaQuery.of(context).size.height * 0.2,
          bottom: MediaQuery.of(context).size.height * 0.2,
          left: MediaQuery.of(context).size.width * 0.08,
          right: MediaQuery.of(context).size.width * 0.08,
          child: Material(
            type: MaterialType.transparency,
            child: Container(
              width: MediaQuery.of(context).size.width * 0.8,
              height: MediaQuery.of(context).size.height * 0.7,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Positioned(
                  top: MediaQuery.of(context).size.height * 0.05,
                  left: MediaQuery.of(context).size.width * 0.03,
                  right: MediaQuery.of(context).size.width * 0.03,
                  child: Column(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    //   crossAxisAlignment: CrossAxisAlignment.start,

                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.01,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.01,
                                0),
                            child: Text(
                              'MỘT SẢN PHẨM CỦA PAL VIỆT NAM',
                              style:
                              TextStyle(fontSize: 17, color: Colors.black),
                            )),
                        CustomPaint(
                          size: Size(MediaQuery.of(context).size.width * 0.8,
                              MediaQuery.of(context).size.height * 0.01),
                          painter: MyPainter(),
                        ),
                        Container(
                            padding: EdgeInsets.fromLTRB(
                                MediaQuery.of(context).size.width * 0.01,
                                MediaQuery.of(context).size.height * 0.02,
                                MediaQuery.of(context).size.width * 0.01,
                                0),
                            width: 200,
                            height: 200,
                            child: Image.asset('assets/images/tuha_pages.png')),
                        Text(
                          'PALBOX',
                          style: TextStyle(fontSize: 17, color: Colors.black),
                        )
                      ])),
            ),
          ),
        )
      ],
    );
  }

  const AnimatedBackground({
    Key key,
  }) : super(key: key);
}
