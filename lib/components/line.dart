import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyPainter extends CustomPainter { //         <-- CustomPainter class
  @override
  void paint(Canvas canvas, Size size) {
      final p1 = Offset(19, 5);
      final p2 = Offset(311, 5);
      final paint = Paint()
        ..color = Colors.blue
        ..strokeWidth = 2;
      canvas.drawLine(p1, p2, paint);
  }

  @override
  bool shouldRepaint(CustomPainter old) {
    return false;
  }
}